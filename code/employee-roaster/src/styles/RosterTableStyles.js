import styled from "styled-components";

export const TableHeading = styled.h2`
  font-size: 1.5em;
  margin-bottom: 10px;
`;
export const EmployeeTable = styled.table`
  width: 90%;
  border-collapse: collapse;
  margin: 20px auto;
`;

export const TableHeader = styled.th`
  cursor: pointer;
  padding: 8px;
  text-align: center;
  border: 1px solid #ddd;
`;

export const TableCell = styled.td`
  border: 1px solid #ddd;
  padding: 8px;
`;

// export const containerStyle = styled.div`
//   display: "flex",
//   flexDirection: "column",
//   alignItems: "flex-start",
//   `;

export default {
  TableHeading,
  EmployeeTable,
  TableHeader,
  TableCell,
  // containerStyle,
};
