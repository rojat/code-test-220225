import styled from "styled-components";

export const PaginationContainer = styled.div`
  display: flex;
  justify-content: right;
  margin-top: 1rem;
  margin-bottom: 20px;
  margin-right: 74px;
`;

export const PaginationButton = styled.button`
  color: #fff;
  border: none;
  padding: 0.5rem 1rem;
  margin: 0 0.5rem;
  cursor: pointer;
  font-size: 1rem;
  color: black;

  &:disabled {
    background-color: #ccc;
    cursor: not-allowed;
  }
`;

export const PageNumberButton = styled.button`
  background-color: #fff;
  border: 1px solid;
  padding: 0.5rem 0.8rem;
  margin: 0 0.3rem;
  cursor: pointer;
  font-size: 1rem;
  color: black;

  &:disabled {
    cursor: not-allowed;
  }
`;
