import styled from "styled-components";

export const AppContainer = styled.div`
  text-align: center;
  font-family: Arial, sans-serif;
`;

export const Heading1 = styled.h1`
  color: #ddd;
`;

export const companyStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  marginTop: "20px",
  paddingBottom: "20px",
  borderBottom: "1px solid white",
  paddingLeft: "40px",
};

export const companyMottoEstStyle = {
  display: "flex",
  justifyContent: "space-between",
  width: "100%",
};

export const containerStyle = {
  border: "2px solid papayawhip",
  margin: "40px",
};
