import styled from "styled-components";

export const PopupContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 5px;
`;

export const CloseButton = styled.button`
  position: absolute;
  top: 10px;
  right: 10px;
`;

export const AvatarImage = styled.img`
  vertical-align: middle; /* Aligns the image vertically */
  margin-right: 10px; /* Adds some spacing to the right of the image */
`;

export const Details = styled.div`
  font-size: 20px;
`;
