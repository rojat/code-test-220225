import React from "react";
import { render } from "@testing-library/react";
import Employee from "../components/Employee";

// Mock the sample data
jest.mock("../data/sample-data.json", () => ({
  companyInfo: {
    companyName: "Test Company",
    companyMotto: "Test Motto",
    companyEst: "2021-09-01",
  },
  employees: [],
}));

test("renders company name and motto", () => {
  const { getByText } = render(<Employee />);

  const companyNameElement = getByText(/Test Company/i);
  const companyMottoElement = getByText(/Test Motto/i);

  expect(companyNameElement).toBeInTheDocument();
  expect(companyMottoElement).toBeInTheDocument();
});

test("renders formatted company established date", () => {
  const { getByText } = render(<Employee />);

  const formattedDateElement = getByText(/Since \(\d{2}-\d{2}-\d{4}\)/i);

  expect(formattedDateElement).toBeInTheDocument();
});
