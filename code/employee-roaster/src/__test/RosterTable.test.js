import React from "react";
import { render, fireEvent } from "@testing-library/react";
import RosterTable from "../components/RosterTable";

const mockData = {
  employees: [
    {
      id: 1,
      firstName: "John",
      lastName: "Doe",
      contactNo: "123-456-7890",
      address: "123 Main St",
    },
  ],
};

test("renders RosterTable component", () => {
  const { getByPlaceholderText, getByText, getAllByText } = render(
    <RosterTable data={mockData} />
  );

  // Check if the table headers are rendered
  expect(getByText(/ID/)).toBeInTheDocument();
  expect(getByText(/Name/)).toBeInTheDocument();
  expect(getByText(/ContactNo/)).toBeInTheDocument();
  expect(getByText(/Address/)).toBeInTheDocument();

  // Check if the search input and button are rendered
  const searchInput = getByPlaceholderText(/Search.../);
  const searchButton = getAllByText(/Search/)[0]; // Use getAllByText and select the first one

  expect(searchInput).toBeInTheDocument();
  expect(searchButton).toBeInTheDocument();

  // Mock a search action
  fireEvent.change(searchInput, { target: { value: "John Doe" } });
  fireEvent.click(searchButton);

  // Check if search is working
  expect(getByText(/John Doe/)).toBeInTheDocument();
});
