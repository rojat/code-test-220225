import React from "react";
import { render } from "@testing-library/react";
import PopupWindow from "../components/PopupWindow";

const mockEmployee = {
  firstName: "John",
  lastName: "Doe",
  jobTitle: "Developer",
  age: 30,
  dateJoined: "2023-10-05",
  bio: "Lorem ipsum dolor sit amet.",
};

test("renders PopupWindow component", () => {
  const handleClose = jest.fn(); // Mocking onClose function

  const { getByText } = render(
    <PopupWindow employee={mockEmployee} onClose={handleClose} />
  );

  const closeButtonElement = getByText(/X/);
  const firstNameElement = getByText(/John/);
  const jobTitleElement = getByText(/Developer/);
  const ageElement = getByText(/30/);
  const dateJoinedElement = getByText(/05-10-2023/);
  const bioElement = getByText(/Lorem ipsum dolor sit amet./);

  expect(closeButtonElement).toBeInTheDocument();
  expect(firstNameElement).toBeInTheDocument();
  expect(jobTitleElement).toBeInTheDocument();
  expect(ageElement).toBeInTheDocument();
  expect(dateJoinedElement).toBeInTheDocument();
  expect(bioElement).toBeInTheDocument();
});
