import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

xtest("renders App component", () => {
  const { getByText } = render(<App />);
  const appHeaderElement = getByText(/Employee/i);
  expect(appHeaderElement).toBeInTheDocument();
});
