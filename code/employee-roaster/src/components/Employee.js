import React, { useState, useEffect } from "react";
import rosterFileData from "../data/sample-data.json";
import RosterTable from "./RosterTable";
import {
  companyMottoEstStyle,
  companyStyle,
  containerStyle,
} from "../styles/employeeStyles";

const Employee = () => {
  const [rosterData, setRosterData] = useState([]);

  useEffect(() => {
    setRosterData(rosterFileData);
  }, []);

  const companyInfo = rosterData.companyInfo || {};

  const companyEstDate = new Date(companyInfo.companyEst);
  const formattedCompanyEst = `${companyEstDate
    .getDate()
    .toString()
    .padStart(2, "0")}-${(companyEstDate.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${companyEstDate.getFullYear()}`;

  console.log(
    "test",
    `${companyEstDate.getDate().toString().padStart(2, "0")}`
  );

  return (
    <div style={containerStyle}>
      <div style={companyStyle}>
        <h1>{companyInfo.companyName}</h1>
        <div style={companyMottoEstStyle}>
          <div>{companyInfo.companyMotto}</div>
          <div>Since ({formattedCompanyEst})</div>
        </div>
      </div>

      <RosterTable data={rosterData} />
    </div>
  );
};

export default Employee;
