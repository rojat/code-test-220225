import React, { useState, useEffect, useRef } from "react";
import {
  TableHeading,
  EmployeeTable,
  TableHeader,
  TableCell,
} from "../styles/RosterTableStyles";
import _ from "lodash";
import avatar from "../image/avatar.png";
import ReactPaginate from "react-paginate";
import {
  PaginationContainer,
  PaginationButton,
  PaginationText,
  PageNumberButton,
} from "../styles/PaginationStyles";
import PopupWindow from "./PopupWindow";
import { AvatarImage } from "../styles/PopupWindowStyles";

const RosterTable = ({ data }) => {
  const [sortedField, setSortedField] = useState(null);
  const [sortOrder, setSortOrder] = useState(null);
  const [pageNumber, setPageNumber] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredEmployees, setFilteredEmployees] = useState([]);
  const entriesPerPage = 5; // Set the number of entries per page

  let employees = data.employees || [];

  const handlePageClick = ({ selected }) => {
    setPageNumber(selected);
  };

  const pageCount = Math.ceil(filteredEmployees.length / entriesPerPage);

  let sortedData = _.sortBy(filteredEmployees, sortedField);

  if (sortOrder === "desc") {
    sortedData = sortedData.reverse();
  }

  const offset = pageNumber * entriesPerPage;
  const currentEntries = sortedData.slice(offset, offset + entriesPerPage);
  const currentEntriesCount = currentEntries.length;
  const totalDataCount = filteredEmployees.length;

  const handleSort = (field) => {
    if (sortedField === field) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortedField(field);
      setSortOrder("asc");
    }
  };

  if (sortedField && sortOrder) {
    sortedData = _.orderBy(sortedData, [sortedField], [sortOrder]);
  }

  const [selectedEmployee, setSelectedEmployee] = useState(null);

  const handleAvatarClick = (employee) => {
    setSelectedEmployee(employee);
  };

  const handleClosePopup = () => {
    setSelectedEmployee(null);
  };

  const searchContainerStyle = {
    display: "flex",
    marginBottom: "10px",
    float: "right",
    marginTop: "20px",
    marginRight: "88px",
    alignItems: "center",
  };

  const containerStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  };

  const searchInputStyle = {
    marginRight: "10px",
  };

  const imageStyle = {
    float: "left",
  };

  const searchStyle = {
    marginLeft: "70px",
    justifyContent: "space-between",
    float: "left",
    marginTop: "10px",
    fontSize: "20px",
  };

  const handleSearch = () => {
    const filteredEmployees = employees.filter((employee) => {
      const fullName = `${employee.firstName} ${employee.lastName}`;
      return fullName.toLowerCase().includes(searchTerm.toLowerCase());
    });

    setSortedField(null);
    setSortOrder(null);
    setPageNumber(0);
    setFilteredEmployees(filteredEmployees);
  };

  useEffect(() => {
    setFilteredEmployees(employees);
  }, [employees]);

  return (
    <div>
      <span style={searchStyle}>
        Searching {currentEntriesCount} of {totalDataCount}
      </span>
      <div style={searchContainerStyle}>
        <input
          type="text"
          style={searchInputStyle}
          placeholder="Search..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <button onClick={handleSearch}>Search</button>
      </div>
      <EmployeeTable>
        <thead>
          <tr>
            <TableHeader
              onClick={() => handleSort("id")}
              className={sortedField === "id" ? `sorted-${sortOrder}` : ""}
            >
              ID {sortedField === "id" && (sortOrder === "asc" ? "↑" : "↓")}
            </TableHeader>
            <TableHeader
              onClick={() => handleSort("firstName")}
              className={
                sortedField === "firstName" ? `sorted-${sortOrder}` : ""
              }
            >
              Name
              {sortedField === "firstName" && sortOrder === "asc" ? "↑" : "↓"}
            </TableHeader>
            <TableHeader
              onClick={() => handleSort("contactNo")}
              className={
                sortedField === "contactNo" ? `sorted-${sortOrder}` : ""
              }
            >
              ContactNo
              {sortedField === "contactNo" && sortOrder === "asc" ? "↑" : "↓"}
            </TableHeader>
            <TableHeader
              onClick={() => handleSort("address")}
              className={sortedField === "address" ? `sorted-${sortOrder}` : ""}
            >
              Address
              {sortedField === "address" && sortOrder === "asc" ? "↑" : "↓"}
            </TableHeader>
          </tr>
        </thead>
        <tbody>
          {currentEntries.map((employee) => (
            <tr key={employee.id}>
              <TableCell>{employee.id}</TableCell>
              <TableCell>
                <img
                  src={avatar}
                  style={imageStyle}
                  alt="Your Image"
                  width="50"
                  height="50"
                  onClick={() => handleAvatarClick(employee)}
                />
                {`${employee.firstName} ${employee.lastName}`}
              </TableCell>
              <TableCell>{employee.contactNo}</TableCell>
              <TableCell>{employee.address}</TableCell>
            </tr>
          ))}
        </tbody>
      </EmployeeTable>

      {selectedEmployee && (
        <PopupWindow employee={selectedEmployee} onClose={handleClosePopup} />
      )}
      <PaginationContainer>
        <PaginationButton
          onClick={() => handlePageClick({ selected: pageNumber - 1 })}
          disabled={pageNumber === 0}
        >
          Prev
        </PaginationButton>

        {Array.from({ length: pageCount }, (_, i) => (
          <PageNumberButton
            key={i}
            onClick={() => handlePageClick({ selected: i })}
            disabled={i === pageNumber}
          >
            {i + 1}
          </PageNumberButton>
        ))}

        <PaginationButton
          onClick={() => handlePageClick({ selected: pageNumber + 1 })}
          disabled={pageNumber === pageCount - 1}
        >
          Next
        </PaginationButton>
      </PaginationContainer>
    </div>
  );
};

export default RosterTable;
