import React, { useState, useEffect } from "react";
import datafile from "../data/sample.json";

const test = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData(datafile);
  }, []);

  const otherInfo = data.otherInfo || {};

  const locStyle = {
    fontSize: "20px",
  };

  return (
    <div style={locStyle}>
      {otherInfo.location}
      {otherInfo.company}
    </div>
  );
};

export default test;
