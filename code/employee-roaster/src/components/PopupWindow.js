import {
  PopupContainer,
  CloseButton,
  AvatarImage,
  Details,
} from "../styles/PopupWindowStyles";
import avatar from "../image/avatar.png";

const PopupWindow = ({ employee, onClose }) => {
  const containerStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    marginTop: "20px",
    fontSize: "20px",
  };

  const bioStyle = {
    flex: "1",
    paddingLeft: "300px",
    textAlign: "justify",
    display: "flex", // Add this line
    flexDirection: "column", // Add this line
    justifyContent: "space-between", // Add this line
    marginTop: "-130px",
    color: "black",
  };

  const avatarStyle = {
    display: "flex",
    flexDirection: "column",
    color: "black",
  };

  const jobTitleStyle = {
    marginBottom: "5px",
    textAlign: "justify",
    color: "black",
  };

  const nameStyle = {
    borderBottom: "1px solid black",
    paddingBottom: "10px",
    color: "black",
  };

  const empDateJoined = new Date(employee.dateJoined);
  const formattedempDateJoined = `${empDateJoined
    .getDate()
    .toString()
    .padStart(2, "0")}-${(empDateJoined.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${empDateJoined.getFullYear()}`;

  return (
    <PopupContainer>
      <CloseButton onClick={onClose}>X</CloseButton>
      <div style={containerStyle}>
        <AvatarImage src={avatar} alt="Your Image" width="70" height="70" />
        <div style={avatarStyle}>
          <div style={jobTitleStyle}>{employee.jobTitle}</div>
          <div>{employee.age}</div>
          <div>{formattedempDateJoined}</div>
        </div>
      </div>
      <div style={bioStyle}>
        <h4 style={nameStyle}>
          {employee.firstName} {employee.lastName}
        </h4>
        <Details>{employee.bio}</Details>
      </div>
    </PopupContainer>
  );
};

export default PopupWindow;
